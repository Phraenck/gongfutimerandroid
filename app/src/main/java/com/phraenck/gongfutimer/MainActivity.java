package com.phraenck.gongfutimer;

import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ITimer {

    private TeaTimer teaTimer;
    private NotificationHandler notificationHandler;

    private TextView txtTimeLeft;
    private TextView lblNext;
    private TextView lblNumBrews;
    private ProgressBar prgProgress;
    private Button btnReset;
    private Button btn1MinCooldown;
    private FloatingActionButton fab;
    private EditText txtTimeAdded;
    private TextView lblInitial;
    private EditText txtInitial;
    private RadioButton radGongFu;
    private RadioButton radWestern;
    private TextView lblTimeAdded;
    private TextView lblRefill;
    private EditText txtRefill;
    private Button btnMinusTime;
    private Button btnPlusTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar ab = getSupportActionBar();
        if(ab != null)
            ab.setTitle("Tea Timer");

        txtTimeLeft = findViewById(R.id.txtTimeLeft);
        lblNext = findViewById(R.id.lblNext);
        lblNumBrews = findViewById(R.id.lblNumBrews);
        prgProgress = findViewById(R.id.prgProgress);
        btnReset = findViewById(R.id.btnReset);
        btn1MinCooldown = findViewById(R.id.btn1MinCooldown);
        fab = findViewById(R.id.fab);
        txtTimeAdded = findViewById(R.id.txtTimeAdded);
        lblInitial = findViewById(R.id.lblInitial);
        txtInitial = findViewById(R.id.txtInitial);
        radGongFu = findViewById(R.id.radGongFu);
        radWestern = findViewById(R.id.radWestern);
        lblTimeAdded = findViewById(R.id.lblTimeAdded);
        lblRefill = findViewById(R.id.lblRefill);
        txtRefill = findViewById(R.id.txtRefill);
        btnMinusTime = findViewById(R.id.btnMinusTime);
        btnPlusTime = findViewById(R.id.btnPlusTime);

        teaTimer = new TeaTimer(this, this, TeaTimer.TimerMode.GONG_FU);

        //fab::OnClick
        fab.setOnClickListener(this::fab_listener);

        //btnReset::OnClick
        btnReset.setOnClickListener(this::btnReset_listener);

        //btn1MinCooldown::OnClick
        btn1MinCooldown.setOnClickListener(this::btn1MinCooldown_listener);

        //txtTimeAdded::OnEditorActionListener
        txtTimeAdded.setText(teaTimer.getAdditionalTimeStr());
        txtTimeAdded.setOnEditorActionListener(this::txtTimeAdded_OnEditorActionListener);

        txtInitial.setText(teaTimer.getInitialTimeStr());
        txtInitial.setOnEditorActionListener(this::txtInitial_OnEditorActionListener);

        txtRefill.setText(String.valueOf(teaTimer.getRefillReminderCt()));
        txtRefill.setOnEditorActionListener(this::txtRefill_OnEditorActionListener);

        btnMinusTime.setOnClickListener(this::btnMinusTime_listener);
        btnPlusTime.setOnClickListener(this::btnPlusTime_listener);

        //toggle modes
        radGongFu.setOnCheckedChangeListener((sender, checkSt) -> {
            if(checkSt)
                setGongFuMode();
        });

        radWestern.setOnCheckedChangeListener((sender, checkSt) -> {
            if(checkSt)
                setWesternMode();
        });

        //setup notifications
        notificationHandler = new NotificationHandler(this);

        updateLabels();
    }

    /**
     * Set the timer to run the timer in western mode
     */
    private void setWesternMode() {
        setGongFuControlVisibilty(View.INVISIBLE);
        teaTimer.setTimerMode(TeaTimer.TimerMode.WESTERN);
        lblTimeAdded.setText(getString(R.string.lblIntervalWestern));
        txtTimeAdded.setText(teaTimer.getLastRunTimeStr());
    }

    /**
     * Set the timer to run in Gong-Fu mode
     */
    private void setGongFuMode() {
        setGongFuControlVisibilty(View.VISIBLE);
        teaTimer.setTimerMode(TeaTimer.TimerMode.GONG_FU);
        lblTimeAdded.setText(getString(R.string.lblIntervalGongFu));
        txtTimeAdded.setText(teaTimer.getLastRunTimeStr());
    }

    /**
     * Make the Gong-Fu specific controls visible or invisible (used when toggling western mode)
     * @param visibility
     */
    private void setGongFuControlVisibilty(int visibility) {
        lblNext.setVisibility(visibility);
        lblNumBrews.setVisibility(visibility);
        lblRefill.setVisibility(visibility);
        btn1MinCooldown.setVisibility(visibility);
        lblInitial.setVisibility(visibility);
        txtInitial.setVisibility(visibility);
        txtRefill.setVisibility(visibility);
    }

    /**
     * Update additionalTimeMs when txtTimeAdded is updated
     * @param textView
     * @param actionId
     * @param event
     * @return
     */
    private boolean txtTimeAdded_OnEditorActionListener(TextView textView, int actionId, KeyEvent event) {
        return this.processTxtTimeAdded(textView);
    }

    /**
     * Update additionalTimeMs when txtTimeAdded is updated
     * @param textView
     * @return
     */
    private boolean processTxtTimeAdded(TextView textView) {
        textView.setError(null);
        String time = textView.getText().toString();

        try {
            teaTimer.setAdditionalTime_mmss(time);
            updateLabels();
        } catch (Exception ex) {
            textView.setError(ex.getMessage());
        }

        return true;
    }

    /**
     * Update initialMs when txtInitial is updated
     * @param textView
     * @param actionId
     * @param event
     * @return
     */
    private boolean txtInitial_OnEditorActionListener(TextView textView, int actionId, KeyEvent event) {
        textView.setError(null);
        String time = textView.getText().toString();

        try {
            teaTimer.setInitialTime_mmss(time);
            updateLabels();
        } catch (Exception ex) {
            textView.setError(ex.getMessage());
        }

        return true;
    }

    /** Set tea timer's number of refills before reminding the user to refill the kettle
     * @param textView
     * @param actionId
     * @param event
     * @return
     */
    private boolean txtRefill_OnEditorActionListener(TextView textView, int actionId, KeyEvent event) {
        textView.setError(null);
        String numRefillCt = textView.getText().toString();

        try {
            int numRefillCtIntCnv = Integer.parseInt(numRefillCt);

            teaTimer.setRefillReminder(numRefillCtIntCnv);
        } catch (Exception ex) {
            textView.setError(ex.getMessage());
        }

        return true;
    }

    /**
     * Click handler for fab
     * Run timer
     * @param view
     */
    private void fab_listener(View view) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        fab.hide();
        txtTimeAdded.setEnabled(false); //disable txtTimeAdded until the timer is finished
        txtInitial.setEnabled(false); //disable txtInitial until the user clicks the reset button
        btn1MinCooldown.setVisibility(View.INVISIBLE);
        teaTimer.runNextTimer();
    }

    /**
     * btnReset_listener
     * Cancel timer, reset elapsedMs to INIT_TIME, and reset controls
     * @param view
     */
    private void btnReset_listener(View view) {

        DialogInterface.OnClickListener listener = (DialogInterface dialog, int which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                teaTimer.reset();

                txtTimeLeft.setText(getString(R.string.initialTime));
                prgProgress.setProgress(0);
                updateLabels();
                fab.show();
                txtTimeAdded.setEnabled(true);
                txtInitial.setEnabled(true);
                btn1MinCooldown.setVisibility(View.VISIBLE);

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.confirm_resetTimer))
                .setPositiveButton(getString(R.string.confirm_yes), listener)
                .setNegativeButton(getString(R.string.confirm_no), listener)
                .show();
    }

    private void btn1MinCooldown_listener(View view) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        fab.hide();
        txtTimeAdded.setEnabled(false);
        btn1MinCooldown.setVisibility(View.INVISIBLE);
        teaTimer.cooldown1Minute();
    }

    /**
     * Subtract 5 seconds from txtTimeAdded
     * @param view
     */
    private void btnMinusTime_listener(View view) {
        this.btnPlusMinusTime(-5000);
    }

    /**
     * Add 5 seconds from txtTimeAdded
     * @param view
     */
    private void btnPlusTime_listener(View view) {
        this.btnPlusMinusTime(5000);
    }

    /**
     * Either add or subtract time from txtTimeAdded
     * @param timeDeltaMs Time to add/ subtract in milliseconds to apply to txtTimeAdded
     */
    private void btnPlusMinusTime(int timeDeltaMs) {
        try {
            int timeMs = TimeConverter.getMsFromTimeStr(txtTimeAdded.getText().toString());
            timeMs += timeDeltaMs;
            timeMs = Math.max(timeMs, 1000);

            txtTimeAdded.setText(TimeConverter.getTimeStrFromMilli(timeMs));
            processTxtTimeAdded(txtTimeAdded);
        } catch (Exception ignored) {
            //eat error
        }
    }

    /**
     * ITimer callback for when TeaTimer ticks
     * @param ms milliseconds until timer completes
     */
    public void onTick(long ms) {
        prgProgress.setProgress(teaTimer.getPercentComplete());
        txtTimeLeft.setText(teaTimer.getTimeString());
    }

    /**
     * ITimer callback for when TeaTimer finishes
     */
    public void onFinish() {
        txtTimeLeft.setText(getString(R.string.done));
        prgProgress.setProgress(0);
        updateLabels();

        fab.show();
        txtTimeAdded.setEnabled(true);
        btn1MinCooldown.setEnabled(true);
        btn1MinCooldown.setVisibility(View.VISIBLE);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        notificationHandler.sendTeaDoneNotification(getString(R.string.timer_done_notification_header), getString(R.string.timer_done_notification_body));
    }

    /**
     * ITimer callback for when water cooldown finishes
     */
    public void onCooldownFinish() {
        txtTimeLeft.setText(getString(R.string.done));
        prgProgress.setProgress(0);

        fab.show();
        txtTimeAdded.setEnabled(true);
        btn1MinCooldown.setEnabled(true);
        btn1MinCooldown.setVisibility(View.VISIBLE);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        notificationHandler.sendTeaDoneNotification(getString(R.string.timer_cooldown_done_notification_header), getString(R.string.timer_cooldown_done_notification_body));
    }

    @Override
    public void refillReminder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.refill_reminder))
                .setPositiveButton(getString(R.string.confirm_ok), (DialogInterface dialog, int which) -> {
                    teaTimer.resetInfusionsSinceLastRefill();
                })
                .show();
    }

    /**
     * Update lblNext and lblNumBrews
     */
    private void updateLabels() {
        lblNext.setText(teaTimer.getTimeString());
        lblNumBrews.setText(getString(R.string.numBrews, teaTimer.getNumBrews()));
    }
}