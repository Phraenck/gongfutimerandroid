package com.phraenck.gongfutimer;

import com.phraenck.gongfutimer.TimeConverter;

import android.content.Context;
import android.os.CountDownTimer;

import org.jetbrains.annotations.NotNull;

/**
 * Implementation of TeaTimer
 * Represents a timer for Gong Fu tea brewing
 *
 * The length the timer runs increases each time it is run.  For example, if additionalTimeMs is set
 * to 10000, the timer will run for '10 * n' seconds where n is the number of times the timer has
 * already been run.
 */
public class TeaTimer {

    public enum TimerMode { GONG_FU, WESTERN }

    private ITimer eventsreciever;
    private TeaTimerSettings settings;
    private CountDownTimer timer;
    private String timestring;
    private int percentComplete = 0;
    private int infusionsSinceRefill = 0;
    private boolean forceRefillReminder = false;

    private TimerMode timerMode;


    /**
     * Initialize new TeaTimer
     * @param eventsreciever Object containing ITimer events that are used when running timer
     */
    TeaTimer(Context context, ITimer eventsreciever, TimerMode timerMode) {
        this.eventsreciever = eventsreciever;
        settings = new TeaTimerSettings(context);
        updateTimeStr();
        this.timerMode = timerMode;
    }

    /**
     * Set timer mode to either gong fu or western
     * @param timerMode New timer mode
     */
    void setTimerMode(TimerMode timerMode) {
        this.timerMode = timerMode;
    }

    /**
     * Get mm:ss formatted time string
     * @return mm:ss formatted time string
     */
    String getTimeString() {
        return timestring;
    }

    /**
     * Get 0-100 percent complete
     * @return int [0..100] representing percentage of timer complete
     */
    int getPercentComplete() {
        return percentComplete;
    }

    /**
     * Get how many milliseconds have elapsed
     * Does not reset after timer finishes running
     * @return elapsed time in milliseconds
     */
    long getElapsedMs() { return settings.getElapsedMs(); }

    /**
     * Get the number of brews since the last reset
     * @return number of brews since the last reset
     */
    int getNumBrews() { return settings.getNumbrews(); }

    /**
     * Get how many milliseconds added to the timer each run
     * @return milliseconds added to the timer each run
     */
    int getAdditionalTimeMs() { return settings.getAdditionalTimeMs(); }

    /**
     * Set how much time should be added each run of the timer
     * @param timestring "mm:ss" formatted string
     * @throws Exception thrown if timestring is invalid
     */
    void setAdditionalTime_mmss(String timestring) throws Exception {
        int prevAdditional = settings.getAdditionalTimeMs();
        int time = TimeConverter.getMsFromTimeStr(timestring);
        settings.setAdditionalTimeMs(time);

        //reset to 10 seconds if <= 0
        if (settings.getAdditionalTimeMs() <= 0)
            settings.setAdditionalTimeMs(10000);

        //store time setting
        if(timerMode == TimerMode.GONG_FU)
            settings.setLastgongfu(time + settings.getInitialtimeMs() - prevAdditional);
        else
            settings.setLastwestern(time);

        updateTimeStr();

        settings.saveSettings();
    }

    /**
     * Set how long the timer should be run for the first run of the timer
     * @param timestring "mm:ss" formatted string
     * @throws Exception thrown if timestring is invalid
     */
    void setInitialTime_mmss(String timestring) throws Exception {
        int time = TimeConverter.getMsFromTimeStr(timestring);
        settings.setInitialtimeMs(time);

        //reset to 20 seconds if <= 0
        if(settings.getInitialtimeMs() <= 0)
            settings.setInitialtimeMs(20000);

        updateTimeStr();
        settings.saveSettings();
    }

    /** Set the number of infusions that should happen before triggering a refill reminder
     * @param infusionsBeforeReminderCt Number of infusions that should happen before triggering a refill reminder
     * @throws Exception Thrown if infusionsBeforeReminderCt < 0
     */
    void setRefillReminder(int infusionsBeforeReminderCt) throws Exception{
        if(infusionsBeforeReminderCt < 0)
            throw new Exception("The number of infusions before refilling the kettle must be positive.");

        settings.setRefillReminderCt(infusionsBeforeReminderCt);

        if(eventsreciever != null && infusionsBeforeReminderCt <= infusionsSinceRefill)
            eventsreciever.refillReminder();
    }

    /**
     * Resets the number of infusions since the last refill
     */
    void resetInfusionsSinceLastRefill() {
        infusionsSinceRefill = 0;
    }

    /**
     * Set timestring to an mm:ss formatted string representing how long the timer will run the next
     * time it is run
     */
    private void updateTimeStr() {
        if(settings.getNumbrews() == 0)
            this.timestring = TimeConverter.getTimeStrFromMilli(settings.getInitialtimeMs());
        else
            this.timestring = TimeConverter.getTimeStrFromMilli(settings.getElapsedMs() + settings.getAdditionalTimeMs());
    }

    /**
     * Get mm:ss formatted string for how much time is added each run of the timer
     * For gong fu, get the amount of time to be added each time the timer runs
     * For western, get the amount of time the timer should run for
     * @return Time string representing the amount of time the timer needs to run for (mm:ss)
     */
    String getLastRunTimeStr() {
        if(timerMode == TimerMode.GONG_FU)
            return TimeConverter.getTimeStrFromMilli(settings.getLastgongfu());
        else
            return TimeConverter.getTimeStrFromMilli(settings.getLastwestern());
    }

    /**
     * Get mm:ss formatted string for how much time will be added per infusion
     * @return Time string representing the amount of time that will be added each infusion
     */
    String getAdditionalTimeStr() {
        return TimeConverter.getTimeStrFromMilli(settings.getAdditionalTimeMs());
    }

    /**
     * Get mm:ss formatted string for the length of the 1st run of the timer
     * @return Time string representing the length of the 1st run of the timer
     */
    String getInitialTimeStr() {
        return TimeConverter.getTimeStrFromMilli(settings.getInitialtimeMs());
    }

    /**
     * Get the number of infusions that should happen before reminding the user to refill the kettle
     * @return The number of infusions that should happen before reminding the user to refill the kettle
     */
    int getRefillReminderCt() { return settings.getRefillReminderCt(); }

    /**
     * Gong Fu:
     * If this is the first time the timer has been run, run the timer for 'initial time' milliseconds.
     * Otherwise, run the timer for 'elapsed time + additional time' milliseconds
     *
     * Western:
     * Run the timer for teh western timer length milliseconds
     */
    void runNextTimer() {
        final int INTERVAL = 1000;
        int additional = settings.getAdditionalTimeMs();
        int initial = settings.getInitialtimeMs();

        //determine the number of milliseconds the timer needs to run for
        long millisInFuture;
        //use initial time if this is the first run in gong fu mode
        if(timerMode == TimerMode.GONG_FU && settings.getNumbrews() == 0)
            millisInFuture = initial;
        //use the total elapsed time + additional if this is a subsequent run in gong fu mode
        else if (timerMode == TimerMode.GONG_FU)
            millisInFuture = settings.getElapsedMs() + additional;
        //use western timer length in western mode
        else {
            millisInFuture = settings.getLastwestern();

            //set forceRefillReminder if the timer was run in western mode and the user switches back to gong-fu mode
            //this way they will get the reminder during the next gong-fu infusion
            forceRefillReminder = true;
        }

        timer = new CountDownTimer(millisInFuture, INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                timestring = TimeConverter.getTimeStrFromMilli(millisUntilFinished);

                //calculate percentage as the time left over the initial time during the 1st brew in gong fu mode
                if(timerMode == TimerMode.GONG_FU && settings.getNumbrews() == 0)
                    percentComplete = (int) ((((millisUntilFinished - INTERVAL) / (double)initial)) * 100);
                //calculate percentage as the time left over the current brew time after the 1st brew in gong fu mode
                else if(timerMode == TimerMode.GONG_FU)
                    percentComplete = (int) (((millisUntilFinished - INTERVAL) / (double) (settings.getElapsedMs() + additional)) * 100);
                //calculate percentage as the time left over the total length of the timer when not in gong fu mode
                else
                    percentComplete = (int) ((millisUntilFinished / (double) millisInFuture) * 100);

                //trigger callback in TeaTimer owner
                if (eventsreciever != null)
                    eventsreciever.onTick(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                if(timerMode == TimerMode.GONG_FU) {
                    //after the 1st brew, our elapsed ms will be equal to the initial time
                    if(settings.getNumbrews() == 0)
                        settings.setElapsedMs(initial);
                    //afterwards it will be equal to itself + the additional ms
                    else
                        settings.setElapsedMs(settings.getElapsedMs() + additional);
                    settings.setNumbrews(settings.getNumbrews() + 1);

                    //trigger refill reminder callback if the number of infusions since the last kettle refill
                    //is >= the setting
                    infusionsSinceRefill++;
                    if(eventsreciever != null && (infusionsSinceRefill >= settings.getRefillReminderCt() || forceRefillReminder)) {
                        eventsreciever.refillReminder();
                        forceRefillReminder = false;
                    }
                }

                updateTimeStr();
                settings.saveSettings();

                //trigger callback in TeaTimer owner
                if (eventsreciever != null)
                    eventsreciever.onFinish();
            }
        };

        timer.start();
    }

    /**
     * Run a one minute timer to allow water to cool down
     */
    void cooldown1Minute() {
        final int INTERVAL = 1000;
        final int MS_1_MINUTE = 60000;

        timer = new CountDownTimer(MS_1_MINUTE, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                timestring = TimeConverter.getTimeStrFromMilli(millisUntilFinished);
                percentComplete = (int)(((double)(millisUntilFinished - INTERVAL) / MS_1_MINUTE) * 100);

                //trigger callback in TeaTimer owner
                if (eventsreciever != null)
                    eventsreciever.onTick(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                //trigger callback in TeaTimer owner
                if(eventsreciever != null)
                    eventsreciever.onCooldownFinish();
            }
        };

        timer.start();
    }

    /**
     * Cancel current timer, reset elapsed time, and reset additional ms to 10000
     */
    void reset() {
        settings.setElapsedMs(0);
        settings.setNumbrews(0);
        if (timer != null)
            timer.cancel();

        settings.saveSettings();

        infusionsSinceRefill = 0;

        updateTimeStr();
    }
}