package com.phraenck.gongfutimer;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Manage settings for TeaTimer
 */
public class TeaTimerSettings {

    private Context context;

    private SharedPreferences preferences;
    private final String ADDITIONALTIME_PREF_KEY = "additionalTimeMs_key";
    private final String STOREDTIME_PREF_KEY = "storedtime_key";
    private final String NUMBREWS_PREF_KEY = "numbrews_key";
    private final String TIMERMODE_PREF_KEY = "timermode_key";
    private final String LASTGONGFU_PREF_KEY = "lastgongfu_key";
    private final String LASTWESTERN_PREF_KEY = "lastwestern_key";
    private final String INITIALTIME_PREF_KEY = "initialtime_key";
    private final String REFILLREMINDER_PREF_KEY = "refillreminder_key";

    private int additionalTimeMs;
    private int numbrews;
    private long elapsedMs;
    private int timermode;
    private long lastgongfu;
    private long lastwestern;
    private int initialTimeMs;

    /**
     * @return Get how many infusions should happen before refilling the kettle
     */
    public int getRefillReminderCt() {
        return refillReminderCt;
    }

    /**
     * @param refillReminderCt Set how many infusions should happen before refilling the kettle
     */
    public void setRefillReminderCt(int refillReminderCt) {
        this.refillReminderCt = refillReminderCt;
    }

    private int refillReminderCt;

    /**
     * @return Get how many milliseconds to add each infusion
     */
    public int getAdditionalTimeMs() { return additionalTimeMs; }

    /**
     * @return Get the total number of infusions
     */
    public int getNumbrews() { return numbrews; }

    /**
     * @return Get the elapsed time of the current timer
     */
    public long getElapsedMs() { return elapsedMs; }

    /**
     * @return Get the saved timer mode
     */
    public int getTimermode() { return timermode; }

    /**
     * @return Get the last timer length in gong fu mode (mm:ss)
     */
    public long getLastgongfu() { return lastgongfu; }

    /**
     * @return Get the last timer length in western mode (mm:ss)
     */
    public long getLastwestern() { return lastwestern; }

    /**
     * @return Get the initial time
     */
    public int getInitialtimeMs() { return initialTimeMs; }


    /**
     * @param additionalTimeMs Set the number of milliseconds to add each infusion
     */
    public void setAdditionalTimeMs(int additionalTimeMs) { this.additionalTimeMs = additionalTimeMs; }

    /**
     * @param numbrews Set the total number of infusions
     */
    public void setNumbrews(int numbrews) { this.numbrews = numbrews; }

    /**
     * @param elapsedMs Set the current elapsed time
     */
    public void setElapsedMs(long elapsedMs) { this.elapsedMs = elapsedMs; }

    /**
     * @param timermode Set the timer mode
     */
    public void setTimerMode(int timermode) { this.timermode = timermode; }

    /**
     * @param lastgongfu Set the last gong fu time (mm:ss)
     */
    public void setLastgongfu(long lastgongfu) { this.lastgongfu = lastgongfu; }

    /**
     * @param lastwestern Set the last western time (mm:ss)
     */
    public void setLastwestern(long lastwestern) { this.lastwestern = lastwestern; }

    /**
     * @param initialTimeMs initial time
     */
    public void setInitialtimeMs(int initialTimeMs) { this.initialTimeMs = initialTimeMs; }


    /**
     * Initialize and load settings
     * @param context context from which to get shared preferences
     */
    public TeaTimerSettings(Context context){
        this.context = context;
        preferences = context.getSharedPreferences("userprefs", Context.MODE_PRIVATE);
        reloadSettings();
    }

    /**
     * Reload all settings
     */
    public void reloadSettings() {
        additionalTimeMs = preferences.getInt(ADDITIONALTIME_PREF_KEY, 10000);
        numbrews = preferences.getInt(NUMBREWS_PREF_KEY, 0);
        elapsedMs = preferences.getLong(STOREDTIME_PREF_KEY, 0);
        timermode = preferences.getInt(TIMERMODE_PREF_KEY, 0);
        lastgongfu = preferences.getLong(LASTGONGFU_PREF_KEY, 1000 * 10);
        lastwestern = preferences.getLong(LASTWESTERN_PREF_KEY, 1000 * 60 * 3);
        initialTimeMs = preferences.getInt(INITIALTIME_PREF_KEY, 20000);
        refillReminderCt = preferences.getInt(REFILLREMINDER_PREF_KEY, 6);
    }

    /**
     * Save all settings
     */
    public void saveSettings() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ADDITIONALTIME_PREF_KEY, additionalTimeMs);
        editor.putInt(NUMBREWS_PREF_KEY, numbrews);
        editor.putLong(STOREDTIME_PREF_KEY, elapsedMs);
        editor.putInt(TIMERMODE_PREF_KEY, timermode);
        editor.putLong(LASTGONGFU_PREF_KEY, lastgongfu);
        editor.putLong(LASTWESTERN_PREF_KEY, lastwestern);
        editor.putInt(INITIALTIME_PREF_KEY, initialTimeMs);
        editor.putInt(REFILLREMINDER_PREF_KEY, refillReminderCt);
        editor.apply();
    }
}