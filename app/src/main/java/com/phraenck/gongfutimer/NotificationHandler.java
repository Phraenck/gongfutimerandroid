package com.phraenck.gongfutimer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

/**
 * Handle registering notification channels and sending notifications for GongFuTimer
 */
public class NotificationHandler {

    Context context;

    public NotificationHandler(Context context) {
        this.context = context;
        createNotificationChannel();
    }

    /**
     * Create notification channel
     */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.notifychannel_name);
            String description = context.getString(R.string.notifychannel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(context.getString(R.string.notifychannel_id), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Send a notification when the timer is done
     * @param header Notification header
     * @param message Notification body
     */
    public void sendTeaDoneNotification(String header, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.notifychannel_id))
                .setSmallIcon(R.drawable.ic_timer_black_24dp)
                .setContentTitle(header)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat.from(context).notify(0, builder.build());
    }
}
